module gitlab.com/trademarkvision/monitoring/kafka_exporter

require (
	github.com/Shopify/sarama v1.17.0
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
	github.com/beorn7/perks v0.0.0-20160804104726-4c0e84591b9a
	github.com/davecgh/go-spew v1.1.0
	github.com/eapache/go-resiliency v1.0.0
	github.com/eapache/go-xerial-snappy v0.0.0-20160609142408-bb955e01b934
	github.com/eapache/queue v1.0.2
	github.com/golang/protobuf v0.0.0-20170920220647-130e6b02ab05
	github.com/golang/snappy v0.0.0-20170215233205-553a64147049
	github.com/krallistic/kazoo-go v0.0.0-20170526135507-a15279744f4e
	github.com/matttproud/golang_protobuf_extensions v1.0.0
	github.com/pierrec/lz4 v0.0.0-20161206202305-5c9560bfa9ac
	github.com/pierrec/xxHash v0.1.1
	github.com/prometheus/client_golang v0.8.0
	github.com/prometheus/client_model v0.0.0-20170216185247-6f3806018612
	github.com/prometheus/common v0.0.0-20170908161822-2f17f4a9d485
	github.com/prometheus/procfs v0.0.0-20170703101242-e645f4e5aaa8
	github.com/rcrowley/go-metrics v0.0.0-20161128210544-1f30fe9094a5
	github.com/samuel/go-zookeeper v0.0.0-20180130194729-c4fab1ac1bec
	github.com/sirupsen/logrus v1.0.3
	golang.org/x/crypto v0.0.0-20170925111901-847319b7fc94
	golang.org/x/sys v0.0.0-20170922123423-429f518978ab
	gopkg.in/alecthomas/kingpin.v2 v2.2.5
)
